A 4 day "out-of-comfort-zone" challenge to use a language and a game framework I never used. 
Tried Lua and the Love2d framework.

The task was to code a Tetris game from scratch, with an AI that tries to always pick the worst tetromino possible for the current board layout.

I think it turned out nice and clean considering how terribly messy and unsafe lua is.

I still need to add a tetromino-sized flood-fill search to cater to a special case issue. Also predicting 1 move in advance would be nice.

============================================================================

I included a portable version of the Sublime editor and configured it to work with an included version of Lua + Love2d.
You can vew the source by opening main.lua with the sublime text editor.

You can also directly play an executable build for Windows directly, by going into 
"\UnfairTetris\UnfairTetris_lua-source\LOVE\UnfairTetrisBuild.exe"

===========================================================================