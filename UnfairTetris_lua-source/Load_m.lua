--to make console output display live while love2d app runs.
io.stdout:setvbuf("no")

--love.load will only get called once when the game is first started (before any other callback)

require "Configs_m"
require "Utility_m"
require "Physics_m"
require "TetrisBoard_m"
require "AM_m"
require "PlayerClass_m"



	





function love.load()
	love.window.setTitle("Tudor's Unfair Tetris")

	--love.window.setMode(800, 600, {resizable=true, vsync=false, minwidth=400, minheight=300})
	success = love.window.setMode( configs.window.width, configs.window.height, {resizable=configs.window.flags.resizable,
																				vsync=configs.window.flags.vsync, 
																				minwidth=configs.window.flags.minwidth, 
																				minheight=configs.window.flags.minheight} )
	if success then
		utils.log("Window. Configured." )
	else
		utils.logError("Invalid window configuration parameters")
	end



	player.onLoad(love.graphics.newImage("images/block_48.png"), {48, 48})

	tetrisBoard.OnLoad(love.graphics.newImage("images/wallBlock_48.png"), {48, 48},
						love.graphics.newImage("images/wallBrick_48.png"), {48, 48},
						love.graphics.newImage("images/debugBlock_48.png"), {48, 48}
						)

	love.graphics.setNewFont(12)
	love.graphics.setBackgroundColor(171,184,230)

	for i=1, #configs.tetrisBoard.grid do
		configs.tetrisBoard.debugGrid[i] = utils.shallow_copy(configs.tetrisBoard.grid[i])
	end
end



