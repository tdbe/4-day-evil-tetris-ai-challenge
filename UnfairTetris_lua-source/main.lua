--[[
    I never used lua or love2d before. wat is dis
    I have no idea what are the best practices with data structures (or, should I say, tables. tables everywhere).
    I mean, it's crazy. I see you can manipulate functions like they're values; what a mad world this is.
--]]

--[[
	I moved the love.load callback from here to Load_m.lua for tidiness. You should start reading that first.

	Also, note that I'm sort of struggling to find a balance between trying coding best practices (from real programming languages), 
	and using lua properly (a language made to be as messy as possible for light top-level scripting).
]]

require "Load_m"
--require "Utility_m"
require "InputMan_m"

local ticker = 0




function love.update(dt)

	if inputManager.isPaused or player.amIDeadYet then
		return
	end

	ticker = ticker + dt
	if ticker >= player.movement.tickSpeed then
		ticker = 0

		--continuous input step
		if love.keyboard.isDown("down") then
		   	player.movement.moveOneDown()
		elseif love.keyboard.isDown("left") then
			player.movement.moveOneLeft()
		elseif love.keyboard.isDown("right") then
			player.movement.moveOneRight()
		else
			--gravity step
			player.movement.moveOneDown()						
		end
		--if love.keyboard.isDown("up") then
		--	player.movement.rotate90()
		--end

		--tetris rules step (check for rows that can be cleared)
		tetrisBoard.clearRows()
	end

	if love.keyboard.isDown("down") or 
		love.keyboard.isDown("left") or 
		love.keyboard.isDown("right") then

    	player.bumpSpeed(-0.9*dt)
    	--utils.log("player.movement.tickSpeed: "..player.movement.tickSpeed)
	end


end



function love.draw()

	tetrisBoard.graphics.renderBoard()

	player.graphics.renderCurrentBlock()
	love.graphics.print("Use the arrow keys. Or don't.\nOh why do humans always bother to delay the inevitable..?", configs.tetrisBoard.cellSize*2+1, 10)

	love.graphics.setNewFont(16)
	love.graphics.print("Here is how terribly you're currently doing: "..scoreboard.score, configs.tetrisBoard.cellSize*3+1, configs.tetrisBoard.cellSize)
	love.graphics.setNewFont(12)
	

	if player.amIDeadYet then
		love.graphics.setNewFont(20)
    	love.graphics.print("Well, I hope this doesn't come to you as a surprise...", configs.tetrisBoard.cellSize*2, configs.tetrisBoard.cellSize*2)
    	love.graphics.setNewFont(12)
	end

end






