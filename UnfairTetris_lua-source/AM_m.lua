--[[
AM, a reference to Harlan Ellison
]]

ai = {}
local private = {}
private.ai = {}
--ai.isEvil = true


--[[
	...
	I can't come up with any elegant recursive solution right now which doesn't get stuck in local minima, can
	detect suspended shelves under overhangs, avoids what's under arches, and doesn't cause a stack overflow :)
	I'm currently dying of an ill-timed cold... But the right way should be a flood fill or A* search for 
	tetromino volumnes...
]]

--[[
function ai.navigator(action, tetrisBlock, blockID, gridPos, rotation, special)


	if action == 'd' then
		
end
]]

function private.saveLocation(blockID, tetrisBlock, x, y, initialBoardEvaluation, pieceScores)

	tetrisBoard.saveTetrisBlock(tetrisBlock, y, x, 1, configs.tetrisBoard.grid)

	local temp = private.ai.evaluateBoard()
	local score = temp - initialBoardEvaluation
	utils.log("initialBoardEvaluation: "..initialBoardEvaluation.." - currentAIEvaluation: "..temp.." = local score: "..score)

	if pieceScores[blockID] == nil or pieceScores[blockID] > score then
			
			pieceScores[blockID] = score
			utils.log ("SCORED: piece id: "..blockID.."; new score: "..score)
	end

	tetrisBoard.eraseTetrisBlock(tetrisBlock, y, x, 1, configs.tetrisBoard.grid)

	return pieceScores
end

function private.getPosAboveTallestPiece()
	local tallestPiece = {i = 0, j = 0}
	for j=1, #configs.tetrisBoard.grid do
		if tallestPiece.i > 0 then
			break
		end
		for k=1, #configs.tetrisBoard.grid[1] do
			if configs.tetrisBoard.grid[j][k] == configs.tetrisBoard.brick then
				tallestPiece.i = j-1
				tallestPiece.j = k
				break
			end
		end
	end
	if tallestPiece.i == 0 then
		tallestPiece.i = configs.tetrisBoard.grid[#configs.tetrisBoard.grid-1] 
		tallestPiece.j = configs.tetrisBoard.grid[1]/2 
	end

	utils.log("tallest piece: i: "..tallestPiece.i..", j: "..tallestPiece.j)
	return tallestPiece
end

--[[
	Returns a number.
	That number represents the least pleasant tetris block for the playerClass to choose.
	It reads all the info it needs (the grid and the block types) from the configs class.
]]
function ai.hitMe(isEvil)

	local initialBoardEvaluation = private.ai.evaluateBoard() --what score does the current board have
	for i=1, #configs.tetrisBoard.debugGrid do
		for j=1, #configs.tetrisBoard.debugGrid[1] do
			configs.tetrisBoard.debugGrid[i][j] = 0
		end
	end


	--[[
	Normally a tetris AI needs to know that height is bad and holes are bad.
	Meaning, you want blocks to be as tightly packed as possible.
	For example, if you take all columns and stack them on top of each other,
	you get a total height. Depending on how many holes are in this stack,
	it can be an efficient height or a less efficient height. And of course any
	height is bad height since we know we can get rid of a row by eliminating
	all holes.
	
	So we need to let the cpu play in advance (once for each piece/rotation), 
	and compare the total height and holes of each boards. 
	Then pick the best or worst one.

	With more time to ponder, and more testing, we can maybe figure out some strategies.
	Right now I can get 1-3 lines in before losing the game.
	
	I definitely want to think more about doing prediction. Proper prediction, as in, 
	predict the player knowing you give them the worst piece knowing you know..
	]]

	local tetrisBlock = {}
	local bestPieceScore = {}

	local pieceScores = {}
	--table.setn(pieceScores, #configs.tetrisBlocks) --damn you, Lua

	--for each piece
	for i=1, #configs.tetrisBlocks do

		tetrisBlock = configs.tetrisBlocks[i]


		local newBlock = utils.cloneTetrisBlock(tetrisBlock)
		local bounds = physics.getBounds(newBlock)
		local newPos = private.getPosAboveTallestPiece()
		newPos.i = newPos.i - math.max(bounds.maxCoord.y, bounds.maxCoord.x) -1
		--[[
		ai.navigator('d', newBlock, i, private.getPosAboveTallestPiece(), 
														{ rots = configs.tetrisBlocks.types[i].rots, 
															currRot = 1})
		]]


		for u = newPos.i, #configs.tetrisBoard.grid -1 do
			for v = 1, #configs.tetrisBoard.grid[1]-1 do

				newBlock = utils.cloneTetrisBlock(tetrisBlock)

				for w = 1, configs.tetrisBlocks.types[i].rots do

					if w > 1 then
						newBlock = physics.rotateTetrisBlock90(newBlock)
					end
					local bounds = physics.getBounds(newBlock)


					if bounds.maxCoord.x+v <= #configs.tetrisBoard.grid[1]-1 and
						bounds.maxCoord.y+u <= #configs.tetrisBoard.grid-1 and
						physics.checkCollisions(v, u, newBlock, configs.tetrisBoard.grid, 1) == false then

						--is it grounded (is there something underneath it)
						if physics.checkCollisions(v, u+1, newBlock, configs.tetrisBoard.grid, 1) == true then

							--[[
								Alright, so here we've determined that we can spawn a piece in an empty area on the map, which
								is also right above ground. 
								But I just circumvented having to travel here from the top of the map. 
								So for all we know, we could have just spawned inside an isolated area (a hole that's completely surrounded)

								So again, what you need here is a currentTetromino-sized A* sweep, to check if we are connected to the ceiling.
								I'll implement this module when I feel better... 
								It's fairly unusual for a player to make enclosed spaces big enough to fit tetrominos in them, 
								so we're kinda fine like this. But if you try to make arches on purpose, you will actually influence the score
							]]
							pieceScores = private.saveLocation(i, newBlock, u, v, initialBoardEvaluation, pieceScores)
						end
					end
				end 
			end
		end
	end

	local max = {val = -999, id = 0}
	for j = 1, #configs.tetrisBlocks.types do
		utils.log("--------------- pieceScores["..j.."]: "..pieceScores[j])
		if max.val < pieceScores[j] then
			max.val = pieceScores[j]
			max.id = j
		end
	end
	
										
	utils.log("AM has chosen this worst piece: "..max.id)
	return max.id

end



function private.ai.evaluateBoard()
	

	local iOfEachPeak = {}
	--table.setn(iOfEachPeak, #configs.tetrisBoard.grid[1])

	local currentHoles = 0
	local currentBrickTotal = 0
	local currentFullRows = 0

	for k=1, #configs.tetrisBoard.grid-1 do
		local thisRowIsFull = true
		for l=2, #configs.tetrisBoard.grid[1]-1 do

			if configs.tetrisBoard.grid[k][l] == configs.tetrisBoard.brick then 
				if iOfEachPeak[l] == nil then
					iOfEachPeak[l]= k
					currentBrickTotal = currentBrickTotal+1
				else
					currentBrickTotal = currentBrickTotal+1
				end
				
			elseif configs.tetrisBoard.grid[k][l] == configs.tetrisBoard.empty then
					
				thisRowIsFull = false

				if iOfEachPeak[l] ~= nil then
					currentHoles = currentHoles+1
				end
			end
		end
		if thisRowIsFull then
			currentFullRows = currentFullRows+1
		end

	end
	
	currentBrickTotal = currentBrickTotal - currentFullRows * #configs.tetrisBoard.grid[1]

	return currentBrickTotal + currentHoles - currentFullRows
end


