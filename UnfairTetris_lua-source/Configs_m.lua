--[[

tables of tables of tables... I feel so unclean!
]]

configs = {}

configs.window = {}

configs.window.width = 672
configs.window.height = 864
configs.window.flags = {resizable=true, vsync=false, minwidth=400, minheight=600}

configs.tetrisBoard = {}
configs.tetrisBoard.cellSize = 48
--configs.tetrisBoard.widthInCells = 10
--configs.tetrisBoard.heightInCells = 20
configs.tetrisBoard.wall = 2
configs.tetrisBoard.brick = 1
configs.tetrisBoard.empty = 0

--default tetris grid should be 10 tiles wide.
--my convention is that 2 means "wall", 1 means "occupied by tetris block", and 0 is empty.
--a 3+ would be an invisible wall
--[[
	I could have probably used a more efficient data structure. Maybe a 1d binary array and
	bit shifting like that Hatetris guy. But IRL, this tetris board is a game map.
	It's  supposed to be a proper discrete representation of more than just boolean values.
	Maye you want a wall, or a block of a certain colour, or a kitchen sink.
]]
configs.tetrisBoard.grid = {
	--{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},--no need for a cap
	--{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	--{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	--{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
	{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}
}
configs.tetrisBoard.debugGrid = {}
--a fresh empty row to clone into the top row of the board, after a row disappears
configs.tetrisBoard.emptyLine = {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2}



configs.difficulty = {}
--how often is the tetris block ticked / updated. (how fast does it fall down)
configs.difficulty.tickSpeed = 0.8

--each block is read into a 2D grid, with origin in top left, 
--and the block only stores the coords to the squares it occupies.
configs.tetrisBlocks = 
					{	--Note: You can totally have custom outrageous tetris blocks of arbitrary shapes and sizes!
						--{ {x=0,y=0}, {x=1,y=0}, {x=1,y=1}, {x=2,y=1}, {x=4,y=1}}, --z
						
						{ {x=0,y=0}, {x=1,y=0}, {x=1,y=1}, {x=2,y=1} }, --z

						{ {x=1,y=0}, {x=2,y=0}, {x=0,y=1}, {x=1,y=1} }, --s

						{ {x=1,y=0}, {x=2,y=0}, {x=1,y=1}, {x=1,y=2} }, --J

						{ {x=0,y=0}, {x=1,y=0}, {x=1,y=1}, {x=1,y=2} }, --L

						{ {x=0,y=0}, {x=1,y=0}, {x=2,y=0}, {x=1,y=1} }, --T

						{ {x=1,y=0}, {x=1,y=1}, {x=1,y=2}, {x=1,y=3} }, --I

						{ {x=0,y=0}, {x=1,y=0}, {x=0,y=1}, {x=1,y=1} }, --# square
					}
configs.tetrisBlocks.types = { --I should have pre-generated a a list of rotations onStart...  
								{kind = 'z', rots = 2},
								{kind = 's', rots = 2},
								{kind = 'j', rots = 2},
								{kind = 'l', rots = 4},
								{kind = 't', rots = 4},
								{kind = 'i', rots = 2},
								{kind = '#', rots = 1} 
							}
--todo: if I remember/have time to implement this, then the program should support arbitrary-sized tetris blocks (ie <4 squares or >4)









