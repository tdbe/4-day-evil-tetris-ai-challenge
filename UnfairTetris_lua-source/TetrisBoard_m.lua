--[[

]]


tetrisBoard = {}
scoreboard = {}
scoreboard.score = 0


tetrisBoard.graphics = {}

local private = {}
private.tileSprite = {
	bitmap = nil, 
	size = {x=0, y=0}
}
private.brickSprite = {
	bitmap = nil, 
	size = {x=0, y=0}
}
private.debugSprite = {
	bitmap = nil, 
	size = {x=0, y=0}
}

function tetrisBoard.OnLoad(arg1, arg2, arg3, arg4, arg5, arg6) --> yes, this could have been done better...

	private.setTileSprite(arg1, arg2)
	private.setDownedBrickSprite(arg3, arg4)
	private.setDebugSprite(arg5, arg6)
end


function tetrisBoard.clearRows()
	
	for y=1, #configs.tetrisBoard.grid-1 do
		local hole = false

		for x=1, #configs.tetrisBoard.grid[y] do
			if configs.tetrisBoard.grid[y][x] == configs.tetrisBoard.empty then
				hole = true
				break
			end
		end

		if hole == false then 
		--means we didn't find any holes in one horizontal row. so it's full. so we delete it
		--we replace this row by shifting down the rows above
			for i=y, 2, -1 do 

				configs.tetrisBoard.grid[i] = configs.tetrisBoard.grid[i-1]
			end

			--get a fresh line for up top.
			configs.tetrisBoard.grid[1] = utils.shallow_copy(configs.tetrisBoard.emptyLine)

			tetrisBoard.youActuallyScored(1)
		end
	end



end


function tetrisBoard.youActuallyScored(amount)

	scoreboard.score = scoreboard.score + amount

end

--wall sprite
function private.setTileSprite(arg1, arg2)

	private.tileSprite.bitmap = arg1
	private.tileSprite.size.x = arg2[1]
	private.tileSprite.size.y = arg2[2]
	--utils.log(configs.window.height .. " " .. private.tileSprite.size.x .. " " .. private.tileSprite.size.y)
end

--what the brick looks like after it's been placed (once it stops moving)
function private.setDownedBrickSprite(arg1, arg2)

	private.brickSprite.bitmap = arg1
	private.brickSprite.size.x = arg2[1]
	private.brickSprite.size.y = arg2[2]
end

function private.setDebugSprite(arg1, arg2)

	private.debugSprite.bitmap = arg1
	private.debugSprite.size.x = arg2[1]
	private.debugSprite.size.y = arg2[2]
end

function tetrisBoard.graphics.renderBoard()
	--for variable = beginning, finish, step do
	if private.tileSprite.bitmap ~= nil and private.brickSprite.bitmap ~= nil then
		for y=1, #configs.tetrisBoard.grid do
			for x=1, #configs.tetrisBoard.grid[y] do

				if configs.tetrisBoard.debugGrid[y][x] <0 then
					love.graphics.draw(private.debugSprite.bitmap, x*configs.tetrisBoard.cellSize, (y-1)*configs.tetrisBoard.cellSize)
				end

				if configs.tetrisBoard.grid[y][x] == configs.tetrisBoard.wall then

					love.graphics.draw(private.tileSprite.bitmap, x*configs.tetrisBoard.cellSize, (y-1)*configs.tetrisBoard.cellSize)
				
				elseif configs.tetrisBoard.grid[y][x] == configs.tetrisBoard.brick then

					love.graphics.draw(private.brickSprite.bitmap, x*configs.tetrisBoard.cellSize, (y-1)*configs.tetrisBoard.cellSize)
				end
				
			end
		end
	else
		utils.logError("TetrisBoard Sprite resources not set!")
	end
end

							
function tetrisBoard.saveTetrisBlock(tetrisBlock, currentPositionX, currentPositionY, cellSize, tetrisBoardGrid)
	tx = 0
	ty = 0

	for i=1, #tetrisBlock do
		
		tx = currentPositionX/cellSize + tetrisBlock[i].x
		ty = currentPositionY/cellSize + tetrisBlock[i].y
		utils.log("saving block... tetrisBlock["..i.."].["..tetrisBlock[i].x.."]["..tetrisBlock[i].y.."]; tetrisBoardGrid["..ty.."]["..tx.."] ")
		if ty > 0 and tx >0 and -- ty <= #tetrisBoardGrid and
		--	tx <= #tetrisBoardGrid[1] and 
			tetrisBoardGrid[ty][tx] == configs.tetrisBoard.empty then

			tetrisBoardGrid[ty][tx] 
									= 
									configs.tetrisBoard.brick

			--this is ugly but I'm running out of time
			--it's only here to print out those blue boxes that look pretty and tell you where the search happened
			configs.tetrisBoard.debugGrid[ty][tx] = -1
		end
	end
	utils.log("\n")
end

--Note: this function does not know whether what it is erasing does indeed belong to the block you tell it to erase
--only use if you're sure these are the blocks you're looking for.
function tetrisBoard.eraseTetrisBlock(tetrisBlock, currentPositionX, currentPositionY, cellSize, tetrisBoardGrid)
	tx = 0
	ty = 0
	
	
	for i=1, #tetrisBlock do
		
		tx = currentPositionX/cellSize + tetrisBlock[i].x
		ty = currentPositionY/cellSize + tetrisBlock[i].y
		utils.log("erasing block... tetrisBlock["..i.."].["..tetrisBlock[i].x.."]["..tetrisBlock[i].y.."]; tetrisBoardGrid["..ty.."]["..tx.."] ")
		if ty > 0 and tx >0 and -- ty <= #tetrisBoardGrid and
		--	tx <= #tetrisBoardGrid[1] and 
			tetrisBoardGrid[ty][tx] == configs.tetrisBoard.brick then
			
			tetrisBoardGrid[ty][tx] 
									= 
									configs.tetrisBoard.empty
			
			
		end
	end
	utils.log("\n")
end