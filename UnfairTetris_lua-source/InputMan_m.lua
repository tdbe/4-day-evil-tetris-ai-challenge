--[[

l
Left Mouse Button.
m
Middle Mouse Button.
r
Right Mouse Button.
wd
Mouse Wheel Down.
wu
Mouse Wheel Up.
x1
Mouse X1 (also known as button 4).
x2
Mouse X2 (also known as button 5).

]]

inputManager = {}

inputManager.isPaused = false;


--[[
function love.mousepressed(x, y, button)
   if button == 'l' then
      --player.movement.moveToCentered(x, y)--debug
   end
end
]]


function love.keypressed(key)

	if inputManager.isPaused or player.amIDeadYet then
		return
	end

   if key == 'down' then
   		player.resetSpeed()
    	player.movement.moveOneDown()
   elseif key == 'left' then
   		player.resetSpeed()
   		player.movement.moveOneLeft()
   elseif key == 'right' then
   		player.resetSpeed()
   		player.movement.moveOneRight()
   	elseif key == 'up' then
   		player.movement.rotate90()
   end
end


function love.keyreleased(key)

	if player.amIDeadYet then
		return
	end

   	if key == 'down' or key == 'left' or key == 'right' then
   		--if the tick speed was increased by holding down the Down key, now we reset it
    	player.resetSpeed()
  	end
end


function love.focus(f)
  if not f then
    print("LOST FOCUS")
  	inputManager.isPaused = true
  else
    print("GAINED FOCUS")
    inputManager.isPaused = false
  end
end


function love.quit()
  print("Thanks for playing! Come back soon!")
end






