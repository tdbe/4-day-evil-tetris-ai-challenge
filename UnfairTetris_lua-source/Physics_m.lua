--[[


]]


physics = {}

--[[
Checks if a tetrisBlock can fit in a grid. Works with both pixel and tile coordinates
]]
function physics.checkCollisions(x, y, tetrisBlock, grid, cellSize)

	--here I'm getting the grid-space coordinates for each tile in the current tetris block
	--then looking them up in the configs.tetrisBoard.grid, to see if they're intersecting anything.
	for i=1, #tetrisBlock do
		local xcoord = x/cellSize + tetrisBlock[i].x
		local ycoord = y/cellSize + tetrisBlock[i].y

		if ycoord < 1 then --I just realized that tetris blocks should move even when mostly off-screen...
			if grid[1][xcoord] --so I pretend they're not off-screen, but on the top row, which is empty anyway
							> configs.tetrisBoard.empty then
				return true
			end
		--regular collision
		elseif grid[ycoord][xcoord] ~=nil and grid[ycoord][xcoord] > configs.tetrisBoard.empty then
			--utils.log("Collision detected at ["..ycoord.."]["..xcoord.."]: "..configs.tetrisBoard.grid[ycoord][xcoord].."\n")   
			return true --true means collided!
		end
											
	end

	return false --no intersection/collision
end


--not really a physics check but it belongs more here rather than in the other files/"classes"
--[[
	Returns a rotated copy of the referenced tetris block.
]]
function physics.rotateTetrisBlock90(tetrisBlock, bounds)
	local temp = {}
	
	if bounds == nil then
		bounds = physics.getBounds(tetrisBlock)
	end

	for i=1, #tetrisBlock do

		--temp[i] = {x = bounds.size - tetrisBlock[i].y - 1, y = tetrisBlock[i].x}
		temp[i] = {x = bounds.size - tetrisBlock[i].y, y = tetrisBlock[i].x}
		
	end

	return temp
end


function physics.getBounds(tetrisBlock)
	local bounds = {}
	bounds.size = 0
	bounds.widthInTiles = 0
	bounds.heightInTiles = 0
	--this was written with possible negative coordinates in mind
	--but the engine doesn't create any negative coordinates
	bounds.maxCoord = {x=-100,y=-100}
	bounds.minCoord = {x=100,y=100}

	for i=1, #tetrisBlock do
		if tetrisBlock[i].x > bounds.maxCoord.x then
			bounds.maxCoord.x = tetrisBlock[i].x
		end
		if tetrisBlock[i].x < bounds.minCoord.x then
			bounds.minCoord.x = tetrisBlock[i].x
		end
		if tetrisBlock[i].y > bounds.maxCoord.y then
			bounds.maxCoord.y = tetrisBlock[i].y
		end
		if tetrisBlock[i].y < bounds.minCoord.y then
			bounds.minCoord.y = tetrisBlock[i].y
		end
	end

	bounds.size = math.max(bounds.maxCoord.x, bounds.maxCoord.y)
	bounds.widthInTiles = bounds.maxCoord.x + math.abs(bounds.minCoord.x)
	bounds.heightInTiles = bounds.maxCoord.y + math.abs(bounds.minCoord.y)

	return bounds
end