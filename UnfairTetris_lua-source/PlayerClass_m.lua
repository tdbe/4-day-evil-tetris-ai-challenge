--[[

Placing player in new file because I can have locals here, 
which would act as private fields for all external files that use player.

I guess I shouldn't have bothered to be civilized with this. 
Lua is not designed to be safe and have private fields.

I just thought I'd show you whether I'm careful about which things I expose etc..
You wouldn't want to make everything a public field of a monobehaviour in unity...
]]



player = {}
player.graphics = {}
local private = {}
player.movement = {}

player.amIDeadYet = false

private.tileSprite = {
	bitmap = nil, 
	size = {x=0, y=0}
}

--[[
private.genericSize = {
	size = 3
	widthInTiles = 3,
	heightInTiles = 3
}
]]

private.spawnPosition = {
	x=configs.window.width/2, 
	y=configs.window.height/2
}

player.movement.currentPosition ={
	x=private.spawnPosition.x,
	y=private.spawnPosition.y
}
player.movement.previousPositionY = -99



function private.setTetrisBlock(val)
	player.tetrisBlock = utils.cloneTetrisBlock(configs.tetrisBlocks[val]) --{ {x=0,y=0}, {x=1,y=0}, {x=1,y=1}, {x=2,y=1} }
	
	--here I'm measuring the bounding box of the current object.
	--useful for when you rotate the pieces for example
	local genericSize = physics.getBounds(player.tetrisBlock)

	private.spawnPosition.x = math.abs((configs.window.width/configs.tetrisBoard.cellSize)/2)*configs.tetrisBoard.cellSize
							 - ((genericSize.widthInTiles*configs.tetrisBoard.cellSize)-configs.tetrisBoard.cellSize)
	private.spawnPosition.y = -configs.tetrisBoard.cellSize*genericSize.heightInTiles
end


function private.initDifficulty()
	player.movement.tickSpeed = configs.difficulty.tickSpeed
	--TODO: possibly increase AI difficulty here. Maybe it recursively thinks 2 moves ahead when it picks the worst block
end


function player.onLoad(arg1, arg2)
	--I should probaby start with the least helpful block, but it's really dull to not at least start with something random
	private.setTetrisBlock(love.math.random( 1, 7 ))

	private.initDifficulty()

	private.setTileSprite(arg1, arg2)

	player.movement.currentPosition.x = private.spawnPosition.x
	player.movement.currentPosition.y = private.spawnPosition.y
end

function player.proceedWithNewBlock()
	private.setTetrisBlock(ai.hitMe(true))
	--private.setTetrisBlock(love.math.random( 1, 7 )) --regular tetris
	player.movement.resetPosition()
	player.resetSpeed()


end

function private.setTileSprite(arg1, arg2)

	private.tileSprite.bitmap = arg1
	private.tileSprite.size.x = arg2[1]
	private.tileSprite.size.y = arg2[2]
	--utils.log(configs.window.height .. " " .. private.tileSprite.size.x .. " " .. private.tileSprite.size.y)
end


function player.graphics.renderCurrentBlock()

	if private.tileSprite.bitmap ~= nil then
		for i=1, #player.tetrisBlock do

			love.graphics.draw(private.tileSprite.bitmap,
								player.movement.currentPosition.x + player.tetrisBlock[i].x*configs.tetrisBoard.cellSize, 
								player.movement.currentPosition.y + player.tetrisBlock[i].y*configs.tetrisBoard.cellSize -configs.tetrisBoard.cellSize)
		end
	end
end


--[[
I'm using a "timer" every frame to move the player (aka tetris block) down by a tile.
Changing this timer's tick speed means block falls faster or slower. 
Useful for instance while you hold down the Down key
]]
function player.bumpSpeed(speedBump)
	--todo: maybe somethingmore clever like a curve function
	player.movement.tickSpeed = player.movement.tickSpeed + speedBump
end
function player.resetSpeed()
	player.movement.tickSpeed = configs.difficulty.tickSpeed
end


function player.movement.resetPosition()
	player.movement.currentPosition.x = private.spawnPosition.x
	player.movement.currentPosition.y = private.spawnPosition.y
end

function player.movement.moveToCentered(x, y)--debug
	player.movement.currentPosition.x = x - (private.genericSize.widthInTiles*configs.tetrisBoard.cellSize)/2
	player.movement.currentPosition.y = y - (private.genericSize.heightInTiles*configs.tetrisBoard.cellSize)/2
end

function player.movement.moveTo(x, y)
	--first we check if the move is legal (if we're not out of bounds or if we're not intersecting anything)
	if physics.checkCollisions(x, y, player.tetrisBlock, configs.tetrisBoard.grid, configs.tetrisBoard.cellSize) then
		return
	end

	player.movement.currentPosition.x = x
	player.movement.currentPosition.y = y
end

function player.movement.moveOneDown()
	player.movement.moveTo(
		player.movement.currentPosition.x,
		player.movement.currentPosition.y + configs.tetrisBoard.cellSize
		)


	player.movement.landingAttempt()

	player.movement.previousPositionY = player.movement.currentPosition.y
end

function player.movement.landingAttempt()

	--it means we're grounded. Drop this brick and get a new one from the top
	if player.movement.currentPosition.y == player.movement.previousPositionY then

		tetrisBoard.saveTetrisBlock(player.tetrisBlock, player.movement.currentPosition.x,
														 player.movement.currentPosition.y, 
									configs.tetrisBoard.cellSize, configs.tetrisBoard.grid)

		if player.movement.currentPosition.y/configs.tetrisBoard.cellSize <= 1 then
			--blimey we lost. who knew?
			player.amIDeadYet = true
		else	
			player.proceedWithNewBlock()
		end
	end

	
end

function player.movement.moveOneLeft()

	player.movement.moveTo(
		player.movement.currentPosition.x - configs.tetrisBoard.cellSize,
		player.movement.currentPosition.y
		)
	
end

function player.movement.moveOneRight()

	player.movement.moveTo(
		player.movement.currentPosition.x + configs.tetrisBoard.cellSize,
		player.movement.currentPosition.y
		)
	
end

--{ {x=0,y=0}, {x=1,y=0}, {x=1,y=1}, {x=2,y=1} }
function player.movement.rotate90()
	local blockSize
	local temp = {}

	temp = physics.rotateTetrisBlock90(player.tetrisBlock)


	--[[
		I attempt a rotatin and then before applying it I check for collisions.
		You know how in tetris games you can keep flipping a block even if it's next to a wall or floor?
		I do that
	]]
	local tempx = player.movement.currentPosition.x + configs.tetrisBoard.cellSize
	local tempy = player.movement.currentPosition.y

	if physics.checkCollisions(player.movement.currentPosition.x, player.movement.currentPosition.y, 
								temp, configs.tetrisBoard.grid, configs.tetrisBoard.cellSize) then

		--we bump it to the right by 1 and see if it fits there
		if physics.checkCollisions(tempx, tempy, 
									temp, configs.tetrisBoard.grid, configs.tetrisBoard.cellSize) then
			--we bump it to the left by 1 and see if it fits there
			tempx = tempx - 2* configs.tetrisBoard.cellSize
			if physics.checkCollisions(tempx, tempy, 
									temp, configs.tetrisBoard.grid, configs.tetrisBoard.cellSize) then
				--we bump it one up and check that
				tempx = tempx + configs.tetrisBoard.cellSize
				tempy = tempy - configs.tetrisBoard.cellSize
				if physics.checkCollisions(tempx, tempy, 
									temp, configs.tetrisBoard.grid, configs.tetrisBoard.cellSize) then

					return
				else
					player.movement.currentPosition.x = tempx
					player.movement.currentPosition.y = tempy
				end
			else
				player.movement.currentPosition.x = tempx
				player.movement.currentPosition.y = tempy
			end
		else
			player.movement.currentPosition.x = tempx
			player.movement.currentPosition.y = tempy
		end

	end


	--finally, assign the new rotated data to the player, if any rotation was possibru
	player.tetrisBlock = temp

end



















